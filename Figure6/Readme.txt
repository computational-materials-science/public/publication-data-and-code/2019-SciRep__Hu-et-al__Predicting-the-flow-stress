The directory named Figure 6 contains two subfolders.

Experiment: experimental data from reference 34 in the paper 
The 1st column is strain rate in s-1
The 2nd column is flow stress in MPa
The 3rd column in the experiment data is the error bar (sdv).


Analytical_model: flow stress predicted by our analytical models
The 1st column is strain rate in s-1
The 2nd column is flow stress in MPa


