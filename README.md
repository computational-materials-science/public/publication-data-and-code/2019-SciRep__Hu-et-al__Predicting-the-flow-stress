# 2019-SciRep__Hu-et-al__Predicting-the-flow-stress

This is the repository for supplementary code and data for the publication 
"Predicting the flow stress and dominant yielding mechanisms: analytical models 
based on discrete dislocation plasticity" by Jianqiao Hu, Hengxu Song, Zhanli 
Liu, Zhuo Zhuang, Xiaoming Liu and Stefan Sandfeld, accepted for publication in Scientific Reports,
2019.

